/*****************************************************************
 *仿佛兮若轻云之蔽月，飘飘兮若流风之回雪
 *@filename MapUtil.java
 *@author WYY
 *@date 2013年11月10日
 *@copyright (c) 2013  wyyft@163.com All rights reserved.
 *****************************************************************/
package com.featherlike.framework.common.util;

import java.util.HashMap;

import org.apache.commons.collections.MapUtils;

public class MapUtil extends MapUtils {
	public static <K, V> HashMap<K, V> newHashMap() {
		return new HashMap<K, V>();
	}
}
