package com.featherlike.feather.generator;

import java.util.List;
import java.util.Map;

import com.featherlike.feather.generator.entity.Column;
import com.featherlike.feather.generator.entity.Table;

public class GeneratorMain {
	public static void main(String[] args) throws Exception {
		IGenerator generator = new GeneratorImpl();
		Map<Table, List<Column>> tableMap = generator.getTableMapFromExcel();
		generator.generateAll(tableMap);
	}
}
